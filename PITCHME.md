#HSLIDE?image=background.jpg

## TI - AEN Financeiro
#### Lean & Agile Nexxera Framework
##### (Excelência Técnica)

#HSLIDE
## A Esteira de desenvolvimento!
![Image-Absolute](steps/desenho-esteira.jpg)

#HSLIDE

## Tip!
For best viewing experience press **F** key to go fullscreen.

#HSLIDE

## Markdown Slides
<span style="font-size:0.6em; color:gray">Press Down key for details.</span> |
<span style="font-size:0.6em; color:gray">See <a href="https://github.com/gitpitch/gitpitch/wiki/Slide-Markdown" target="_blank">GitPitch Wiki</a> for details.</span>


#VSLIDE

#### Use GitHub Flavored Markdown
#### For Slide Content Creation

<br>

The same tool you use to create project **READMEs** and **Wikis** for your Git repos.